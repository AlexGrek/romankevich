prprob =1.2 * 10e-4
aprob = 1.1 * 10e-4
cprob = 1.9 * 10e-4
dprob = 3.2 * 10e-5
bprob = 1.4 * 10e-5
mprob = 3.3 * 10e-4

elements = {
    'pr1': {
        'name': 'Pr1',
        'failure_probability': prprob
    },
    'pr2': {
        'name': 'Pr2',
        'failure_probability': prprob
    },
    'pr3': {
        'name': 'Pr3',
        'failure_probability': prprob
    },
    # 'pr4': {
    #     'name': 'Pr4',
    #     'failure_probability': prprob
    # },
    # 'pr5': {
    #     'name': 'Pr5',
    #     'failure_probability': prprob
    # },
    'pr6': {
        'name': 'Pr6',
        'failure_probability': prprob
    },
    'a1': {
        'name': 'A1',
        'failure_probability': aprob
    },
    'a2': {
        'name': 'A2',
        'failure_probability': aprob
    },
    'a3': {
        'name': 'A3',
        'failure_probability': aprob
    },
    'c1': {
        'name': 'C1',
        'failure_probability': cprob
    },
    'c2': {
        'name': 'C2',
        'failure_probability': cprob
    },
    'c4': {
        'name': 'C4',
        'failure_probability': cprob
    },
    'c5': {
        'name': 'C5',
        'failure_probability': cprob
    },
    'c6': {
        'name': 'C6',
        'failure_probability': cprob
    },
    # 'c9': {
    #     'name': 'C9',
    #     'failure_probability': cprob
    # },
    'd1': {
        'name': 'D1',
        'failure_probability': dprob
    },
    'd2': {
        'name': 'D2',
        'failure_probability': dprob
    },
    'd3': {
        'name': 'D3',
        'failure_probability': dprob
    },
    'd6': {
        'name': 'D6',
        'failure_probability': dprob
    },
    'd7': {
        'name': 'D7',
        'failure_probability': dprob
    },
    'd8': {
        'name': 'D8',
        'failure_probability': dprob
    },
    # 'c1_1': {
    #     'name': 'C1',
    #     'failure_probability': cprob
    # },
    # 'c2_1': {
    #     'name': 'C2',
    #     'failure_probability': cprob
    # },
    # 'c4_1': {
    #     'name': 'C4',
    #     'failure_probability': cprob
    # },
    # 'c5_1': {
    #     'name': 'C5',
    #     'failure_probability': cprob
    # },
    # 'd1_1': {
    #     'name': 'D1',
    #     'failure_probability': dprob
    # },
    # 'd2_1': {
    #     'name': 'D2',
    #     'failure_probability': dprob
    # },
    # 'd3_1': {
    #     'name': 'D3',
    #     'failure_probability': dprob
    # },
    # 'd6_1': {
    #     'name': 'D6',
    #     'failure_probability': dprob
    # },
    # 'd7_1': {
    #     'name': 'D7',
    #     'failure_probability': dprob
    # },
    # 'd8_1': {
    #     'name': 'D8',
    #     'failure_probability': dprob
    # },
    'b1': {
        'name': 'B1',
        'failure_probability': bprob
    },
    'b3': {
        'name': 'B3',
        'failure_probability': bprob
    },
    'b4': {
        'name': 'B4',
        'failure_probability': bprob
    },
    'b2': {
        'name': 'B2',
        'failure_probability': bprob
    },
    'b5': {
        'name': 'B5',
        'failure_probability': bprob
    },
    # 'b6': {
    #     'name': 'B6',
    #     'failure_probability': bprob
    # },
    'm1': {
        'name': 'M1',
        'failure_probability': mprob
    },
    # 'm2': {
    #     'name': 'M2',
    #     'failure_probability': mprob
    # },
    # 'm9': {
    #     'name': 'M9',
    #     'failure_probability': mprob
    # },
}


elements_mod = {
    'pr1': {
        'name': 'Pr1',
        'failure_probability': prprob
    },
    'pr2': {
        'name': 'Pr2',
        'failure_probability': prprob
    },
    'pr3': {
        'name': 'Pr3',
        'failure_probability': prprob
    },
    # 'pr4': {
    #     'name': 'Pr4',
    #     'failure_probability': prprob
    # },
    # 'pr5': {
    #     'name': 'Pr5',
    #     'failure_probability': prprob
    # },
    'pr6': {
        'name': 'Pr6',
        'failure_probability': prprob
    },
    'a1': {
        'name': 'A1',
        'failure_probability': aprob
    },
    'a2': {
        'name': 'A2',
        'failure_probability': aprob
    },
    'a3': {
        'name': 'A3',
        'failure_probability': aprob
    },
    'c1': {
        'name': 'C1',
        'failure_probability': cprob
    },
    'c2': {
        'name': 'C2',
        'failure_probability': cprob
    },
    'c4': {
        'name': 'C4',
        'failure_probability': cprob
    },
    'c5': {
        'name': 'C5',
        'failure_probability': cprob
    },
    'c6': {
        'name': 'C6',
        'failure_probability': cprob
    },
    'c9': {
        'name': 'C9',
        'failure_probability': cprob
    },
    'd1': {
        'name': 'D1',
        'failure_probability': dprob
    },
    'd2': {
        'name': 'D2',
        'failure_probability': dprob
    },
    'd3': {
        'name': 'D3',
        'failure_probability': dprob
    },
    'd6': {
        'name': 'D6',
        'failure_probability': dprob
    },
    'd7': {
        'name': 'D7',
        'failure_probability': dprob
    },
    'd8': {
        'name': 'D8',
        'failure_probability': dprob
    },
    # 'c1_1': {
    #     'name': 'C1',
    #     'failure_probability': cprob
    # },
    # 'c2_1': {
    #     'name': 'C2',
    #     'failure_probability': cprob
    # },
    # 'c4_1': {
    #     'name': 'C4',
    #     'failure_probability': cprob
    # },
    # 'c5_1': {
    #     'name': 'C5',
    #     'failure_probability': cprob
    # },
    # 'd1_1': {
    #     'name': 'D1',
    #     'failure_probability': dprob
    # },
    # 'd2_1': {
    #     'name': 'D2',
    #     'failure_probability': dprob
    # },
    # 'd3_1': {
    #     'name': 'D3',
    #     'failure_probability': dprob
    # },
    # 'd6_1': {
    #     'name': 'D6',
    #     'failure_probability': dprob
    # },
    # 'd7_1': {
    #     'name': 'D7',
    #     'failure_probability': dprob
    # },
    # 'd8_1': {
    #     'name': 'D8',
    #     'failure_probability': dprob
    # },
    'b1': {
        'name': 'B1',
        'failure_probability': bprob
    },
    'b3': {
        'name': 'B3',
        'failure_probability': bprob
    },
    'b4': {
        'name': 'B4',
        'failure_probability': bprob
    },
    'b2': {
        'name': 'B2',
        'failure_probability': bprob
    },
    'b5': {
        'name': 'B5',
        'failure_probability': bprob
    },
    # 'b6': {
    #     'name': 'B6',
    #     'failure_probability': bprob
    # },
    'm1': {
        'name': 'M1',
        'failure_probability': mprob
    },
    # 'm2': {
    #     'name': 'M2',
    #     'failure_probability': mprob
    # },
    'm9': {
        'name': 'M9',
        'failure_probability': mprob
    },
}

load_table = {
    'pr1': {
        'nominal_load': 40,
        'max_load': 100,
        'redistributions': {
            'pr2': 40,
            'pr3': 20,
            'pr6': 40
        }
    },
    'pr2': {
        'nominal_load': 50,
        'max_load': 120,
        'redistributions': {
            'pr1': 30,
            'pr3': 30,
            'pr6': 50
        }
    },
    'pr3': {
        'nominal_load': 50,
        'max_load': 100,
        'redistributions': {
            'pr1': 10,
            'pr2': 40,
            'pr6': 10
        }
    },
    'pr6': {
        'nominal_load': 50,
        'max_load': 100,
        'redistributions': {
            'pr1': 50,
            'pr2': 50,
            'pr3': 50
        }
    }
}